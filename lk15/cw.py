# Вопросы, которые можно встретить на собеседовании


# -----------------------------------------------------------
# 1. Что из следующего является недопустимым выражением?
# -----------------------------------------------------------

# 1) abc = 1,000,000 +
# 2) abc = 1000 2000 3000 -
# 3) a, b, c = 1000, 2000, 3000 +
# 4) a_b_c = 1,000,000 +
# 5) abc = 1_000_000 *3 +

# -----------------------------------------------------------
# 2. Что будет если выполнить код ?
# -----------------------------------------------------------

# a = [[]]*3
# a[0].append(42)
# print (a)

# Варианты:
# 1) []
# 2) [[],[],[]]
# 3) [[42],[],[]]
# 4) [[42], [42], [42]] +
# 5) ([42], [], [])

# -----------------------------------------------------------
# 3. Напишите, что распечатает код?
# -----------------------------------------------------------

# A0 = dict([('a', 1), ('b', 2), ('c', 3), ('d', 4), ('e', 5)])
# print (A0)
#

# A1 = range(10)
# A2 = sorted([i for i in A1 if i in A0])
# print (A2)
#

# A3 = sorted([A0[s] for s in A0])
# print (A3)


# A4 = [i for i in A1 if i in A3]
# print (A4)
#

# A5 = {i:i*i for i in A1}
# print (A5)
#

# A6 = [[i,i*i] for i in A1]
# print (A6)
#

# -----------------------------------------------------------
# 4. Что распечатает код:
# -----------------------------------------------------------

# def f(x,l=[]):
#     for i in range(x):
#       l.append(i*i)
#     print(l)
#
# f(2)
#
# Варианты:
# 1) [0, 1]
# 2) []
#
#

# f(3,[3,2,1])
#
# Варианты:
# 1) [0, 1, 4]
# 2) [3, 2, 1, 0, 1, 4]
# 3) [3, 2, 1]
#
#

# f(3)
#
# Варианты:
# 1) [0, 1]
# 2) [0, 1, 4]
# 3) [0, 1, 0, 1, 4]
#
#

# -----------------------------------------------------------
# 5. Что распечатает код? В чем разница между выражениями?
# -----------------------------------------------------------

# a = [1,2]
# del a
# print (a)
#
# Варианты:
# 1) []
# 2) ошибка
# 3) [1, 2]
#
#

# a = [1,2]
# del a[:]
# print (a)
#
# Варианты:
# 1) []
# 2) ошибка
# 3) [1, 2]
#


# -----------------------------------------------------------
# 6. Каким будет результат кода ниже?
# -----------------------------------------------------------

# def multipliers():
#     return [lambda x : i * x for i in range(4)]
#
# print ( [m(2) for m in multipliers()] )

# Варианты:
# 1) [6, 6, 6, 6]
# 2) [0, 2, 4, 6]
# 3) [4, 4, 4, 4]
#


# -----------------------------------------------------------
# 7. Что произойдет в этих двух фрагментах и почему?
# -----------------------------------------------------------

# a = [1, 2, 3, 4, 5]
# for x in a:
#   del x
# print(a)
#
# Варианты:
# 1) []
# 2) [1, 2, 3, 4, 5]
#

# a = [1, 2, 3, 4]
# while a:
#     del a[0]
# print(a)
#
# Варианты:
# 1) []
# 2) [1, 2, 3, 4, 5]
#

# -----------------------------------------------------------
# 8. Каким будет результат кода ниже?
# -----------------------------------------------------------

# list = ['a', 'b', 'c', 'd', 'e']
# print (list[10:])
#
# Варианты:
# 1) ['a', 'b', 'c', 'd', 'e']
# 2) ['a']
# 3) []
# 4) ['a', 'b', 'c', 'd', 'e', 'a', 'b', 'c', 'd', 'e']
#

# -----------------------------------------------------------
# 9. Будет ли работать код ниже? Почему “да” или почему “нет”?
# -----------------------------------------------------------

# class DefaultDict(dict):
#     def __missing__(self, key):
#         return []
#
# d = DefaultDict()
# d['florp']
#

# -----------------------------------------------------------
# 10. Каким будет результат кода ниже? Обьясните свой ответ.
# -----------------------------------------------------------

# class Parent(object):
#     x = 1
# class Child1(Parent):
#     pass
# class Child2(Parent):
#     pass
# print (Parent.x, Child1.x, Child2.x)

# Варианты:
# 1) 1 1 1
# 2) 1
#
#

# Child1.x = 2
# print (Parent.x, Child1.x, Child2.x)

# Варианты:
# 1) 1 1 1
# 2) 1 2 1
# 3) 2 2 2
#

# Parent.x = 3
# print (Parent.x, Child1.x, Child2.x)

# Варианты:
# 1) 1 1 1
# 2) 3 3 3
# 3) 3 2 3
# 4) 2 2 2
#


# -----------------------------------------------------------
# 11. Чем отличаются () от [] в списковых выражениях?
# -----------------------------------------------------------

# То есть, чем [x for x in [1,2]] отличается от (x for x in [1,2])?


# ------------
import typing as t

input_data = {
    1: 3,
    2: 4,
    3: 3,
    4: {
        1: 3,
        2: 4,
        3: 3,
        4: {
            1: 3,
            2: 4,
            3: 3,
        }
    }
}

DICT_ = t.Dict[t.Union[str, int], t.Union[int, float]]


def convert(data: t.Dict[t.Union[str, int], t.Union[int, float, DICT_]]) -> DICT_:
    for key, value in data.items():
        if isinstance(value, dict):
            data[key] = sum(convert(value).values())
    return data


# print(convert(input_data))
# assert convert(input_data) == {
#     1: 3,
#     2: 4,
#     3: 3,
#     4: 20
# }


# -------

def find_single(nums: t.List[int]) -> int:
    for i in nums:
        if nums.count(i) == 1:
            return i


# assert find_single([4, 1, 2, 1, 2]) == 4
# assert find_single([2, 2, 1]) == 1
# assert find_single([1]) == 1


# ------
import re


def get_last_word_len(string: str) -> int:
    string = re.sub(r'[^A-Za-z ]', ' ', string)
    return len(string.strip().split(' ')[-1])


# assert get_last_word_len("   fly me   to   the moon  ") == 4
# assert get_last_word_len("Hello World") == 5
# assert get_last_word_len('luffy is still joyboy') == 6
# assert get_last_word_len('Some string \xa0 with !;;;_^ special symbols |********|') == 7


# ------
def is_polindrom(word: str) -> bool:
    word_len = len(word)
    for i in range(word_len // 2):
        if word[i] != word[-(i + 1)]:
            return False
    return True


# assert is_polindrom('aaaa')
# assert is_polindrom('pop')
# assert is_polindrom('aaaa1') is False

# -------

def find_nearest(primary: t.List[int], secondary: t.List[int]) -> t.List[t.Tuple[int]]:
    result = []
    for idx, i in enumerate(primary):
        best_index = None
        best_item = None
        best_diff = None
        for jdx, j in enumerate(secondary, start=best_index or 0):
            curr_diff = abs(i - j)
            if best_item is None or best_diff > curr_diff:
                best_index = jdx
                best_item = j
                best_diff = curr_diff
        result.append((i, best_item))
    return result


# assert find_nearest([10, 20, 30], [10, 20, 30]) == [(10, 10), (20, 20), (30, 30)]
# assert find_nearest([1, 20, 30], [1]) == [(1, 1), (20, 1), (30, 1)]
# assert find_nearest([-30, -15, 20, 30], [-20, 10, 15, 40]) == [(-30, -20), (-15, -20), (20, 15),
#                                                                (30, 40)]


# -------

def str_str(string: str, needle: str) -> int:
    if match := re.search(needle, string):
        return match.span()[0]
    return -1

# assert str_str("hello", "ll") == 2
# assert str_str("aaaaa", "bba") == -1
