# I\O

# print('my_list', 'dfkjnfdkjv')
#
# my_input = input('Enter your name:')
#
# print(my_input)
# print(type(my_input))

# LIST
# my_list = [1, 2, 3, 4, 5, 'jfj', [6, 7]]
# my_list.pop(2)
# my_list.insert(2, 5)
# print(my_list)
#
# my_list_2 = [1, 2, 3, 4, 5, 'AAA', [6, 7]]
#
# my_list_2[2] = 76
# my_list_2[-1][0] = 11
#
# print(my_list_2, 'my_list_2')
# print(my_list_2.count(2), 'my_list_2.count')
# print(len(my_list_2), 'my_list_2_len')
# my_list_2.append('HI')
# print(my_list_2, 'my_list_2_append')

# срезы list, str, tuple, set
# str, tuple -> не изменяемые
# my_list_2 = [1, 2, 3, 4, 5, 'AAA', 'dfjknvdjfnvjd']
#
# print(my_list_2[1:5:4])
# print(my_list_2[:])

# my_str = '1234567jjjf'
# print(my_str[1:5:2])
# my_str[0] = 'a'

# TUPLE
# my_tuple = (1, 2, 3, 2, 4, 6)
# print(my_tuple[:5])

# DICTS
# my_dict = {True: 'f', 'dd': 0, 'a': 'b', None: False}
# # my_dict_2 = dict(one=1, two=2)
# print(my_dict)
# # print(my_dict_2)
# print(my_dict[True])
# print(list(my_dict.values()))
# print(my_dict.values())
# print(my_dict.keys())
# print(my_dict.items())
# my_dict[True] = 120
# print(my_dict[True])
# print(len(my_dict))

# SET
# my_set = {1, True, 0, False, 'x', 'a', 'c'}
#
# print(my_set)
# print(ord('x'))
# print(chr(120))
# print(ord('a'))
# print(ord('c'))
# my_set.add(100)
# print(my_set)


# Изменяемые типы данных
# list, set, dict


# не изменяемые типы данных
# str, int, None, bool, tuple, float
# from keyword import kwlist
#
# print(kwlist)

# что есть итерабельным - tuple, list, dict, set, str
print('@c' in 'hfhfh@com.ua')
print('@' in list('hfhfh@com.ua'))
print(list('hfhfh@com.ua'))
print(''.join(list('hfhfh@com.ua')))
print('fjfjfjf'.split('j'))


#  как понять какой тип
print(type('jfjfj'))
print(type([]))
print(type(()))
print(type({}))
print(type(set()))

print(type('jfjfj') == str)
print(isinstance('jfjfj', str))
print(help('str'))

# for git ssh-keygen -t rsa -b 2048
