import dataclasses
import random
import typing as t


# разберемся с крестиками - ноликами


# class Coord(t.NamedTuple):
#     row: int
#     col: int


class User(t.NamedTuple):
    name: str
    sign: str


class XO:
    __ALLOWED_SINGS = {'X', '0'}
    __EMPTY = '_'

    def __init__(self):
        self.table = [
            ['_', '_', '_'],
            ['_', '_', '_'],
            ['_', '_', '_'],
        ]
        self.table_map = {
            '1': [0, 0],
            '2': [0, 1],
            '3': [0, -1],
            '4': [1, 0],
            '5': [1, 1],
            '6': [1, -1],
            '7': [-1, 0],
            '8': [-1, 1],
            '9': [-1, -1],
        }

        self.winner_coords = (
            {'1', '2', '3'},
            {'4', '5', '6'},
            {'7', '8', '9'},
            {'1', '4', '7'},
            {'2', '5', '8'},
            {'3', '6', '9'},
            {'1', '5', '9'},
            {'3', '5', '7'},
        )

        self.x_coords = set()
        self.o_coords = set()

    def make_mark(self, field_num, sign):
        if sign not in self.__ALLOWED_SINGS:
            return
        coords = self.table_map[field_num]
        existing_sign = self.table[coords[0]][coords[-1]]
        if existing_sign != self.__EMPTY:
            print(f'Point already allocated with {existing_sign}')
            return
        self.table[coords[0]][coords[-1]] = sign
        if sign == 'X':
            self.x_coords.add(field_num)
        else:
            self.o_coords.add(field_num)

    def is_end(self, user_sing):
        for coord in self.winner_coords:
            if user_sing == 'X':
                if coord.issubset(self.x_coords):
                    return True
            else:
                if coord.issubset(self.o_coords):
                    return True

    def __str__(self):
        return f"{self.table[0][0]}\t | \t{self.table[0][1]}\t | \t{self.table[0][-1]}\n" \
               f"{self.table[1][0]}\t | \t{self.table[1][1]}\t | \t{self.table[1][-1]}\n" \
               f"{self.table[-1][0]}\t | \t{self.table[-1][1]}\t | \t{self.table[-1][-1]}\n"


# def main():
#     xo = XO()
#     first_user_name = input('Input your name')
#     second_user_name = input('Input your name')
#     user_list = [first_user_name, second_user_name]
#     x_user = random.choice(user_list)
#     print(f'First user name {x_user}')
#     user_list.pop(user_list.index(x_user))
#     users = (User(name=x_user, sign='X'), User(name=user_list[0], sign='0'))
#     is_first = False
#     count = 0
#     while True:
#         print(xo)
#         user = users[is_first]
#         print(f'Current user {user.name}')
#         number_of_point = input('Point number')
#         xo.make_mark(field_num=number_of_point, sign=user.sign)
#         count += 1
#         if count >= 3 and xo.is_end(user.sign):
#             print(f'{user.name} if winner')
#             print(xo)
#             return
#         is_first = not is_first
#
#
# main()
# Пасхалка _A__attr = 3
# _A__attr = 3
#
#
# class A:
#     def get(self):
#         return __attr
#
#
# a = A()
#
# print(a.get())

# Будет ли тут ошибка?

# d = {1: 1, 2: 2, 3: 3, 4: 4}
#
# a = list(range(10))
# for ids, i in enumerate(a):
#     if i == 2:
#         a.pop(ids)


# for k, v in d.copy().items():
#     if k == 2:
#         d.pop(k)
#
# print(d)

#  некоторые структуры из ДС

"""
1. Реализовать структуру стек
Стек (англ. stack — стопка; читается стэк) — абстрактный тип данных, представляющий собой 
список элементов, организованных по принципу LIFO.
"""


class stack:
    def __init__(self, initial_list=None):
        self.values = initial_list or []

    def add(self, element):
        self.values.insert(0, element)

    def pop(self):
        if not self.values:
            return None
        return self.values.pop(0)

    def add_to_end(self):
        self.values.append(0)

    def is_empty(self):
        return bool(self.values)


"""
2. Реализовать структуру связанный список
Свя́зный спи́сок — базовая динамическая структура данных в информатике, состоящая из узлов, 
каждый из которых содержит как собственно данные, так и одну или 
две ссылки («связки») на следующий и/или предыдущий узел списка.
"""


@dataclasses.dataclass
class obj:
    value: t.Any
    next_value: t.Any = None

    def __str__(self):
        return f'{self.value} -> {self.next_value}'

    def __repr__(self):
        return f'{self.value} -> {self.next_value}'


class linked_list:
    def __init__(self, initial=None):
        self.values = self.__to_objects(initial or [])

    def __to_objects(self, initial):
        values = []
        for idx, i in enumerate(initial):
            if idx == len(initial):
                values.append(obj(value=i))
                continue
            values.append(obj(value=i, next_value=initial[idx + 1]))
        return values

    def append(self, item):
        if not self.values:
            self.values = [obj(value=item)]
            return
        prev_item = self.values[-1]
        self.values.append(obj(value=item))
        prev_item.next_value = item

    def pop(self, index):
        if index < 0:
            return
        if not self.values:
            return
        if index == 0:
            self.values.pop(index)
        elif len(self.values) - 1 == index:
            self.values.pop(index)
            self.values[index - 1].next_value = None
        else:
            self.values.pop(index)
            self.values[index - 1].next_value = self.values[index + 1].next_value

    def __str__(self):
        return str(self.values)


# ll = linked_list()
#
# ll.append(1)
# ll.append(2)
# ll.append(5)
# ll.pop(2)
# ll.pop(0)
# print(ll)

