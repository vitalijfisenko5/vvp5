# Generators
def some_gen():
    x = 1
    while True:
        yield x
        x += 1
        yield x ** 2

        if x > 15:
            yield None


# gen = some_gen()
# print(gen)
# print(next(gen))
# print(next(gen))
# print(next(gen))
# for i in gen:
#     print(i)
# gen = (i for i in range(10))
#
# for i in gen:
#     print(i)

def process_file(file_name):
    with open(file_name, 'r') as file:
        file_gen = (line for line in file)
        for line in file_gen:
            print(line)


# process_file('../lk7/meest_stat_based_on_delivery_service_data.csv')

def gen_with_param(x):
    while True:
        x = yield x + 1


gen = gen_with_param(5)
print(gen.send(None))
print(gen.send(4))
print(gen.send(10))
gen.throw(ValueError('Stop iteration'))
import random

# gen = (i for i in range(10))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# print(next(gen))
# Регулярные выражения
import re

# print(re.match('\d', 'jksdjkkcjndkfjnv'))
# print(re.fullmatch('1233', '1233'))
# print(re.search('\d', 'jksdjkkcjnd121kfjnv'))

# print(re.match('[А-ЯҐЄІЇ]', 'kjdnfjvndkjfnkv'))
# print(re.match('[А-ЯҐЄІЇ]', 'РПРПРОПОА'))

# Задачи
"""
1. Написать генератор чисел фиббоначи
"""


def fib():
    curr, past = 1, 1
    while True:
        yield curr
        past, curr = curr, curr + past


# gen = fib()
#
# for i in range(10):
#     print(next(gen))

""" SKIP
2. Написать игру "Угадай число".
 - Учасники 2 и больше
"""

"""
3. Дан рандомный список чисел и целевое число. Написать функцию, которая вернет индексы чисел, сумма
которых равна целевому числу
"""


def two_sum(arr, target):
    indexes = []
    for number in arr:
        try:
            indexes = [arr.index(number), arr.index(target - number)]
            break
        except:
            continue
    return indexes


# assert two_sum([2, 7, 6, 12, 48, 9], target=9) == [0, 1]

"""
4. Написать функцию, которая вернет True если строка/число/список являются полиндромами, 
и False если нет
"""


def polindrom(value):
    if isinstance(value, int):
        value = str(value)

    return value == value[::-1]


# print(polindrom('1111'))
# print(polindrom('1112'))
# print(polindrom([1, 2, 2, 1]))
# print(polindrom([1, 2, 2, 2]))

"""
5. Написать функцию, которая вернет саммый длиный "префикс" среди списка строк
"""


def get_prefics(arr):
    if not arr:
        return None
    arr = sorted(arr, key=lambda x: len(x))
    shortest = arr[0]
    end = len(shortest) - 1
    while not all(map(lambda x: x.startswith(shortest), arr)):
        shortest = shortest[:end]
        end -= 1
    return shortest or None


assert get_prefics(['dshhsdh', '1111221', 'kkkkkk']) == None
assert get_prefics(['flower', 'flush', 'flood']) == 'fl'
"""
6. Дан отсортированный список чисел и целевое число. Написать функцию, которая вернет индекс 
целевого числа, а если такого нет, то индекс где оно могло бы стоять
"""


def find_place(arr, target):
    try:
        return arr.index(target)
    except ValueError:
        for item in arr:
            if item > target and arr[arr.index(item) - 1] < target:
                return arr.index(item)
            elif target < item and arr.index(item) == 0:
                return 0
        return len(arr)


# assert find_place([1, 2, 3, 4, 5], target=6) == 5
# assert find_place([1, 2, 3, 4, 5], target=6) == 5
# assert find_place([1, 2, 3, 4, 5, 6, 7, 8], target=6) == 5
# assert find_place([1, 2, 3, 12, 31], target=6) == 3
# assert find_place([1, 2, 3, 12, 31], target=0) == 0
