class Human:
    def __init__(self, name, last_name, age):
        self.name = name
        self.last_name = last_name
        self.age = age
        self.is_walking = False

    def walk(self):
        self.is_walking = True

    def stop(self):
        self.is_walking = False

    def set_age(self, new_age):
        self.age = new_age


# human1 = Human('Ivan', 'Vanov', 56)
# human2 = Human('Bogdan', 'Bogdan', 46)
#
# print(human1.is_walking)
# human1.walk()
# print(human1.is_walking)
# human1.stop()
# print(human1.age)
# human1.set_age(20)
# print(human1.is_walking)
# print(human1.age)


class Mammal:
    def __init__(self, type_: str):
        self.mammal_type = type_

    def move(self):
        print(f'{self.mammal_type.capitalize()} is moving')

    def feed(self):
        print(f'{self.mammal_type.capitalize()} is feeding')


class Monkey(Mammal):
    def __init__(self, mammal_type, name, age, color):
        self.name = name
        self.age = age
        self.color = color
        super().__init__(mammal_type)

    def move(self):
        print(f'{self.mammal_type.capitalize()} is climbing')

    def make_some_noise(self):
        print('AAAA')


# monkey = Monkey('monkey', 'Albert', 9, 'black')
#
#
# monkey.move()
# monkey.make_some_noise()
# monkey.feed()

class ReptileMixin:
    def change_skin(self):
        print(f'{self.mammal_type.capitalize()}"s skin has been changed')


class Reptiloid(ReptileMixin, Monkey):
    def __init__(self, mammal_type, name, age, color):
        super().__init__(mammal_type, name, age, color)


# r = Reptiloid('reptiloid', 'Albert', 150, 'green')
#
# print(Reptiloid.__mro__)
#
# r.change_skin()
# r.move()
# r.feed()

class NewHuman(Human):
    def __init__(self, name, last_name, age):
        self.is_hungry = True
        super().__init__(name, last_name, age)

    def __feed(self):
        self.is_hungry = False
        print('))))')
        print('(((((')

    def feed_pulic(self):
        self.__feed()

    def _some_protected(self):
        print('_some_protected')


# h = NewHuman('Ivan', 'Vanov', 56)
#
# h.walk()
# print(h.is_walking)
# # h._NewHuman__feed()
# h.feed_pulic()
# h._some_protected()
# print(h.is_hungry)


class A(NewHuman):
    pass


# a = A('Ivan', 'Vanov', 56)
#
# print(dir(a))
#
# a._NewHuman__feed()

# m = Mammal('fffff')
#
# monkey = Monkey('monkey', 'Albert', 9, 'black')
#
# for item in [m, monkey]:
#     # getattr(item, 'move')()
#     # item.move()
#     setattr(item, 'new_field', True)
#     print(item.new_field)


class B:
    a = 'kjdjfnvkjndf'

    def __init__(self, name):
        self.name = name
        self.a = B.a


# b = B('yyytyyt')
#
# print(b.name)
# print(b.a)
# print(B.a)
# b.a = 1
# print(b.a)
# print(B.a)


# Задачи

class Figure:
    def perimetr(self):
        raise NotImplementedError

    def square(self):
        raise NotImplementedError


class Pr(Figure):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def perimetr(self):
        return (self.a + self.b) * 2

    def square(self):
        return self.a * self.b


class Triangle(Pr):
    def __init__(self, a, b, c):
        super().__init__(a, b)
        self.c = c

    def perimetr(self):
        return self.a + self.b + self.c

    def square(self):
        half_p = 0.5 * (self.a + self.b + self.c)
        return (half_p * (half_p - self.a) * (half_p - self.b) * (half_p - self.c)) ** 0.5


# t = Triangle(2, 3, 2)
# print(t.square())
# print(t.perimetr())
#
# pr = Pr(2, 2)
# print(pr.square())
# print(pr.perimetr())

def convert(en, valute):
    if valute == 'usd':
        print(int(en) * USD)

    else:
        print(int(en) * EUR)


def main():
    value = ...
    valute = ...

    if not value.isdigit():
        print('kjdfvdkf')
        return

    if value.lowwer() not in {'usd', 'eur'}:
        return

    convert(value, valute)


main()
