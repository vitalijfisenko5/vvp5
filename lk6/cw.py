# Files
# r, r+, w, w+, a, a+
def open_file_simple():
    file = open('first.txt', 'r')

    print(file.read())

    file.close()


# open_file_simple()

def open_file_auto_close():
    with open('first.txt', 'r') as file:
        print(file.read())


# open_file_auto_close()


def write_to_file():
    with open('new.txt', 'w+') as new_file:
        new_file.write('Hello@)))!!!!')


# write_to_file()


def append_to_file():
    with open('new.txt', 'a+') as new_file:
        new_file.write('\n 222222')


# append_to_file()


def open_2_files():
    with open('new_2.txt', 'w') as new_file, open('new.txt', 'r') as old_file:
        new_file.write(old_file.read())


# open_2_files()

def store_in_var():
    with open('new.txt', 'r') as file:
        file_date = file.read()

    print(file_date)


# store_in_var()
from datetime import datetime, timedelta


def log_action(log):
    with open('log.log', 'a') as file:
        file.write(f'\n {log}')


def rize_some_err(err):
    func_name = 'rize_some_err'
    try:
        raise err
    except TypeError:
        log_action(f'TypeError raised at {datetime.now()}, {func_name=}')
    except ZeroDivisionError:
        log_action(f'ZeroDivisionError raised at {datetime.now()}, {func_name=}')


# rize_some_err(TypeError)
# rize_some_err(ZeroDivisionError)
# rize_some_err(None)
# rize_some_err(ValueError)
# Json

import json


def open_json():
    with open('config.json', 'r') as file:
        data = json.load(file)
        print(data)
        print(type(data))


# open_json()

# print(json.loads('{"home_codes": ["068", "096"],"to_foreign": 2,"from_foreign": 3}'))
# print(type(json.loads('{"home_codes": ["068", "096"],"to_foreign": 2,"from_foreign": 3}')))


def w_json():
    with open('new.json', 'w') as file:
        json.dump({'hello': 1}, file, indent=4)


# w_json()
# CSV


"""
Задача 1, но большая)

Написать свою систему биллинга для мобильного оператора

В файле config.json осписать настройки
 - префиксы вашего оператора 068, 096 и тп
 - тариф в сети
 - тариф звонка в другую сеть
 - тариф звонка из другой сети

Написать миханизм записи всех звонков, по окончанию программы записать звонки с файла data.json
"""
import random


def get_config():
    with open('config.json') as file:
        return json.load(file)


def restore_db():
    try:
        with open('call_db.json') as db:
            return json.load(db)
    except FileNotFoundError:
        return {}


CONFIG: dict = get_config()

CALL_DB: dict = restore_db()


def deactivate_app():
    with open('call_db.json', 'w') as file:
        json.dump(CALL_DB, file, indent=4)


def is_home_number(phone_number):
    if phone_number.startswith('+'):
        code = phone_number[3:7]
    else:
        code = phone_number[2:6]
    return code in CONFIG['home_codes']


def get_tariff(is_home):
    if is_home:
        return 0
    return CONFIG['from_foreign']


def track_call(phone_number):
    is_home = is_home_number(phone_number)
    tariff = get_tariff(is_home)
    call_time = random.randrange(1, 10)
    return {
        'full_price': tariff * call_time,
        'tariff': tariff,
        'phone_number': phone_number,
        'call_start': datetime.now().isoformat(),
        'call_end': (datetime.now() + timedelta(minutes=call_time)).isoformat(),
    }


def main():
    is_app_active = True
    while is_app_active:
        phone_number = input('start call, input number: ')
        data = track_call(phone_number)

        if CALL_DB.get(phone_number):
            CALL_DB[phone_number].append(data)
        else:
            CALL_DB[phone_number] = [data]

        deactivate_input = input('Stop app y/yes')
        if deactivate_input in {'y', 'yes'}:
            deactivate_app()
            is_app_active = False


main()
