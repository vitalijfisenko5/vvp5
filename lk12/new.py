

_DATA = {
    'name': 'Ivan',
    'last_name': 'Ivan2',
    'age': 12,
}


def __getattr__(name):
    if name in _DATA:
        return _DATA[name]
    raise AttributeError(f'{name} not found')


