import random
import uuid
from dataclasses import dataclass


class Cart:
    def __init__(self):
        self.items = []

    def __add__(self, other):
        if isinstance(other, CartItem):
            self.items.append(other)
            return
        raise ValueError('Rogue itenm type passed')

    def add_item(self, item):
        self.items.append(item)


class CartItem:
    def __init__(self):
        self.name = str(uuid.uuid4())

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


# cart = Cart()
#
# cart.add_item(CartItem())
# cart + CartItem()
#
# print(cart.items)

class CustomInt(int):
    def __add__(self, other):
        if isinstance(other, int):
            return super().__add__(other)
        return super().__add__(len(other))

    def __hash__(self):
        return random.randint(0, 100)

    def __eq__(self, other):
        return hash(self) == hash(other)


# new_int = CustomInt(4)
#
# print(new_int)
#
# print(new_int + 4)
# print(new_int + [1, 2, 3])

class CallableClass:
    def __call__(self, *args, **kwargs):
        print(args, kwargs)


# some_callable = CallableClass()
# print(some_callable)
# some_callable(1, c='c')

class TryNew:
    __objects = []

    @classmethod
    def __new__(cls, *args, **kwargs):
        self = super().__new__(*args, **kwargs)
        # if cls.objects:
        #     raise Exception('Already created')
        cls.__objects.append(self)
        return self

    def __init__(self):
        self.name = str(uuid.uuid4())

    def __iter__(self):
        return self.__objects.__iter__()

    def __repr__(self):
        return self.name

    def close(self):
        self.__objects.pop()
        del self


for _ in range(10):
    TryNew()

for conn in TryNew():
    conn.close()


# new_set = {CustomInt(1), CustomInt(2), CustomInt(3), CustomInt(4), CustomInt(5), CustomInt(5)}
# print(new_set)

# import new as n
#
# print(type(n))
#
# print(n.name)
# print(n.age)
# print(n.last_name)


# metaclasses

def create_class(name):
    class A:
        pass

    class B:
        pass

    if name == 'a':
        return A
    return B


# print(type('SomeClass', (), {}))


# Задачи

"""
1. Написать метакласс, который не позволит переопределять магические методы в потомках
"""

class MagicGuard(type):
    def __new__(mcs, name, bases, dct):
        for b in bases:
            for method in b.__dict__.keys():
                print(method)
                print(bases)
                if method.startswith('__') and method.endswith('__') and method not in {
                    '__module__',
                    '__dict__',
                    '__weakref__',
                    '__doc__',
                }:
                    raise Exception('Error')

        return super().__new__(mcs, name, bases, dct)


# class Base(metaclass=MagicGuard):
#     pass
#
#
# class A(Base):
#     def __init__(self):
#         self.a = 'a'
#
#
# class B(A):
#     pass
#
#
# b = B()

"""
2. Написать класс, обьект которого ведет себя как словарь и как список
"""


class ListDict(dict):
    def get(self, key):
        if isinstance(key, int):
            try:
                return list(self.values())[key]
            except LookupError:
                return super().__getitem__(key)
        return super().__getitem__(key)

    def append(self, item):
        self[item.id] = item

    def __iter__(self):
        return self.values().__iter__()

    def pop(self, item):
        pass


@dataclass
class Item:
    id: str = uuid.uuid4()


new_dict = ListDict()
item = Item()
new_dict.append(item)

# for item in new_dict:
#     print(item)

# print(new_dict.get(item.id))

"""
3. Реализовать класс Банкомат, у которого есть баланс. Банкомат может 
выдавать деньги и принимать платежи. Банкомат не может уйти в минус и не может 
обрабатывать отрицательные сумму.
"""


class Paument:
    def __init__(self):
        self.value = random.randint(-100, 1000000)
        self.is_negative = self.value < 0

    def __repr__(self):
        return str(self.value)


class ATM:
    def __init__(self):
        self.initial_value = random.randint(100, 1000000)

    def __repr__(self):
        return str(self.initial_value)

    def __iadd__(self, other):
        if not other.is_negative:
            self.initial_value += other.value
            return self.initial_value
        return self.initial_value

    def __isub__(self, other):
        if not other.is_negative and self.initial_value >= other.value:
            self.initial_value -= other.value
            return self.initial_value
        return self.initial_value


# atm = ATM()
#
# payment = Paument()
#
# print(atm)
# print(payment)
#
# atm += payment
#
# print(atm)
# payment_2 = Paument()
# atm -= payment_2
#
# print(atm)

"""
4. Написать класс, в котором можно ограничить кол-во методов
"""

# class Slots:
#     __slots__ = ('div', 'div2')
#
#     def add(self):
#         print('OK')
#
#     def div(self):
#         print('NEOK')


# s = Slots()
