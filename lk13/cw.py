# 1 поток
# import time
# COUNT = 50000000
#
#
# def countdown(n):
#     while n > 0:
#         n -= 1
#
#
# start = time.time()
# countdown(COUNT)
# end = time.time()
#
# print('Затраченное время -', end - start)

# 2 потока
# import time
# from threading import Thread
#
# COUNT = 50000000
#
#
# def countdown(n, tr_name):
#     while n > 0:
#         n -= 1
#
#
# t1 = Thread(target=countdown, args=(COUNT // 2,'first'))
# t2 = Thread(target=countdown, args=(COUNT - COUNT // 2, 'second'))
#
# start = time.time()
# t1.start()
# t2.start()
# t1.join()
# t2.join()
# end = time.time()
#
# print('Затраченное время -', end - start)


# 2 процесса
# from multiprocessing import Pool
# import time
#
# COUNT = 50000000
#
#
# def countdown(n):
#     while n > 0:
#         n -= 1
#
#
# if __name__ == '__main__':
#     pool = Pool(processes=2)
#     start = time.time()
#     r1 = pool.apply_async(countdown, [COUNT // 2])
#     r2 = pool.apply_async(countdown, [COUNT - COUNT // 2])
#     pool.close()
#     pool.join()
#     end = time.time()
#     print('Затраченное время в секундах -', end - start)


# Задачи

"""
1. Замерить скорость выполнения операции скачивания картинорк в 1м потоке и в мультитрединге
"""
import asyncio
import random
from time import time

import aiofiles
import requests
from threading import Thread

# def download(name):
#     resp = requests.get(url='https://picsum.photos/200/300')
#     with open(f'images/{name}.png', 'wb') as f:
#         f.write(resp.content)


# 1.249593734741211

# start = time()
# for i in range(0, 5):
#     Thread(target=download, args=(i,)).start()
# print(time() - start)

"""
2. Замерить скорость выполнения операции скачивания картинорк c аiofiles, aiohttp, asyncio
"""
import aiohttp


async def download(number):
    async with aiohttp.ClientSession() as session:
        resp = await session.get(url='https://picsum.photos/200/300')
        content = await resp.read()
    async with aiofiles.open(f'{number}.png', 'wb') as file:
        await file.write(content)


async def main():
    functions = []
    for i in range(0, 5):
        functions.append(download(i))
    await asyncio.gather(*functions)

#
# start = time()
# asyncio.get_event_loop().run_until_complete(main())
# print(time() - start)

