"""
1. Валидатор паролей. На занятии делали валидацию емейла, тут
    нужно сделать валидацию пароля по такому же принципу.
    Придумать критерии надежного пароля, описать их в условиях.
"""
# special_chr = {'@', '#', '!'}
# passw = input('Enter your password: ')
# if passw.isdigit():
#     print('Invalid')
# elif passw.isalpha():
#     print('Invalid')
# elif passw.islower():
#     print('Invalid')
# elif not special_chr.intersection(set(passw)):
#     print('Invalid')
# else:
#     print('valid')

"""
2. Создать список, где все элементы будут кратные 5ти (упражнение на функцию range)
"""
# print(list(range(5, 101, 5)))
"""
3. Нарисовать в консоли ёлочку)
"""
# cr_tree_list = ['*\n\t' * i for i in range(1, 8)]
# print(''.join(cr_tree_list))
# print('    *    ')
# print('    **   ')
"""
4. Задать число (input или number=Ваше число) и посчитать количество цифр в нем
"""
# number = input('Enter your number: ')
#
# if number.isdigit():
#     print(len(number))
# else:
#     print('Not a number')
"""
5. Сгенерировать произваольный список и развернуть его
"""

# initial_list = [i for i in range(11)]
# print(initial_list[::-1])
# initial_list.reverse()
# print(initial_list)

# P.S

# dict([(1, 2), (3, 4)])
