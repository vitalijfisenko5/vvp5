# разбор дз

# my_str = 'kjdsbvkjdkfbvkjdbf'
#
# print(dict.fromkeys(my_str))
# my_dict = dict(enumerate(my_str))
# print(''.join(my_dict.values()))

# По теме знятия

# IF ELIF ELSE statement

# my_input = input('Enter tour age: ')

# if not my_input.isdigit():
#     print(f'It"s string {my_input}')
# elif all([my_input.isdigit(), int(my_input) >= 18]):
#     print('U are adult')
# else:
#     print('U are not adult')
#
# if my_input == 'aaa':
#     print('New if statement')

# условие? если да, делать это: делать то from JS
# print('равно' if my_input == 'фффф' else 'не равно')  # тнернарный оператор нет elif

"""
На что не сработает if\elif
0, None, False, (), [], {}, set, ''
"""

# For statement

# for idx, char in enumerate('jknsdkvnjkjndf'):
#     print(idx, char)

# for item in range(101):
#     print(item)

# for item in [1, 2, 4, 3.14, 'kfkf', 43, 12, 4.12]:
#     if isinstance(item, str):
#         continue
#     if item == 12:
#         break
#     print(item)


# my_list = [i for i in range(11) if not i % 2]


# first = 'uuuuuuuuuu'
# second = 'uuuuuuuuuua'
# if first != second:
#     for idx, i in enumerate(second):
#         if idx > len(first):
#             print('AAAAAAAA')
#         elif i == first[idx]:
#             print('ok')
#         else:
#             print('not ok')


# print({i: i**i for i in range(10) if i < 4})
# print({i for i in range(10)})

# print((i for i in range(10)))

# i = 0
# cond = True
#
# while cond:
#     i += 1
#     if i == 50:
#         print('continue')
#         continue
#     if i == 100:
#         print('kfkfkf')
#         cond = False
#         break
#     if not cond:
#         print('last loop')

"""
ЗАДАЧКИ
"""
"""
1. Написать программу, которая запрашивает у пользователя емейл и валидирует его.
Нужно убедится что:
 - есть @
 - заканчивается на .uа или .com
"""

# email = input('Enter email: ')
#
# if '@' in email:
#     if email.endswith('.com') and len(email) > 6 and not email.startswith('@'):
#         print('Valid')
#     elif email.endswith('.ua') and len(email) > 5 and not email.startswith('@'):
#         print('Valid')
#     else:
#         print('Invalid')
# else:
#     print('Invalid')

"""
2. Написать программу, которая запросит у пользователя 
возраст и выведет подходящую категорию фильмов
"""

FILMS_MAP = {
    '18+': '18+',
    '16-18': '16-18',
    '0-12': '0-12',
}
# age = input('Enter your age: ')

# if not age.isdigit():
#     print('Ivalid')
# elif int(age) >= 18:
#     print(FILMS_MAP['18+'])
# elif 16 <= int(age) < 18:
#     print(FILMS_MAP['16-18'])
# elif int(age) < 13:
#     print(FILMS_MAP['0-12'])

# if age.isdigit():
#     age = int(age)
# else:
#     print('Ivalid')
#     1/0
#
# if age >= 18:
#     print(FILMS_MAP['18+'])
# elif 16 <= age < 18:
#     print(FILMS_MAP['16-18'])
# elif age < 13:
#     print(FILMS_MAP['0-12'])

"""
3. Написать программу, которая запросит у пользователя строку, и выведет на екран:
 - сообщение с ошибкой если символ не число
 - если число четное - само число и сообщение что оно четное
 - если число нечетное - само число и сообщение что оно нечетное
"""
# number = input('Enter your number: ')
#
# if not number.isdigit():
#     print('Error!')
# elif int(number) % 2:
#     print(f'{number} even')
# else:
#     print(f'{number} odd')

"""
4. Написать программу, которая запросит у пользователя текущий год и будет опрашивать его пока он не 
введен верный результат
"""
# curr_year = '2022'
# while True:
#     user_year = input('Suggest year: ')
#     if curr_year == user_year:
#         print('OK')
#         break

"""
5. Написать калькулятор. Операции и символы запрашивать у пользователя
"""
# operations = ['+', '-', '*', '/']
# op = input(f'Enter OPeration one of {operations}:')
#
# first_d = input('Enter first D :')
# second_d = input('Enter second D :')
#
# if op not in operations:
#     raise ValueError('Invalid operation')
# if not (first_d.isdigit() and second_d.isdigit()):
#     raise ValueError('Invalid numbers')
#
# first_d = int(first_d)
# second_d = int(second_d)
# if op == '+':
#     print(first_d + second_d)
# elif op == '-':
#     print(first_d - second_d)
# elif op == '/':
#     print(first_d / second_d)
# elif op == '*':
#     print(first_d * second_d)

print(eval("2+5+6"))
