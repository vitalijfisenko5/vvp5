from functools import wraps
from inspect import unwrap
import abc


class A:
    def __init__(self):
        self.a = 0


class B:
    def __init__(self):
        self.B = 0


class C(A, B):
    pass


class D:
    def __init__(self):
        self.d = 0


class E(C, D):
    def __init__(self):
        super(B, self).__init__()


def dec(func):
    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)

    return wrapper


@dec
def one_arg_one_kw(a, *args, c=None, **kwargs):
    return a + c


@dec
def one_arg(a):
    return a


def with_param(error: Exception):
    def dec(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except error:
                print('Gon an Error')

        return wrapper

    return dec


@with_param(TypeError)
def raise_value_error():
    raise ValueError


# raise_value_error()

class Mammal(abc.ABC):

    @abc.abstractmethod
    def go(self):
        pass

    @abc.abstractmethod
    def run(self):
        pass

    @abc.abstractmethod
    def sleep(self):
        pass


# class Person(Mammal):
#     def __init__(self, name):
#         self.name = name
#
#     def go(self):
#         pass
#
#     def run(self):
#         pass
#
#     def sleep(self):
#         pass


# p = Person('Ivan')


class Person:
    type_ = 'person'

    def __init__(self, name, age):
        self.name = name
        self.age = age
        self.__job_count = 4

    @property
    def greeting(self):
        return f'Hi I`m {self.name}!'

    @property
    def job_count(self):
        return self.__job_count

    @job_count.setter
    def job_count(self, value):
        raise ValueError('Forbidden')

    @job_count.deleter
    def job_count(self):
        print('Cannot delete')

    # def get_job_count
    #
    # job_count = property(
    #     fset=,
    #     fget=,
    #     fdel=,
    # )

    @classmethod
    def new(cls):
        return cls('Ivan', 23)

    @staticmethod
    def static():
        """
        Не наследуется
        :return:
        """
        return None


# p = Person('Ivan', 23)
# p1 = Person.new()
#
# print(p.greeting)
# p.job_count = 5

# del p.job_count


# Задачи

"""
1. Написать декоратор с параметрами для обработки ошибок
"""

"""
2. Реализовать класс Human
 - дать возможность знакомится с другими 
 - выводить True если такой человек знаком и False если нет
"""


class Human:
    def __init__(self, name):
        self.name = name
        self.known_humans = {}

    @property
    def known_humans(self):
        return self.known_humans

    @known_humans.setter
    def known_humans(self, value):
        self.known_humans = value

    def is_known(self, other):
        return id(other) in self.known_humans

    def know_other(self, other: 'Human'):
        if id(other) not in self.known_humans:
            self.known_humans[id(other)] = other
            other.know_other(self)
            return
        print('Already known')


# a = Human('A')
# b = Human('B')
#
# print(a.is_known(b))
# a.know_other(b)
# print(b.is_known(a))
# print(a.is_known(b))

"""
3. Реализовать программу, которая позволит вести список дел
"""

import typing as t
from dataclasses import dataclass, asdict


@dataclass
class Task:
    date_created: str
    name: str
    is_done: bool = False

    def to_json(self):
        return asdict(self)

    def __str__(self):
        return f'{self.name=} {self.is_done=}'


# class Item(t.NamedTuple):
#     date_created: str
#     text: str
#     is_done: bool
#
#     def to_json(self):
#         return {
#             'date_created': self.date_created,
#             'text': self.text,
#             'is_done': self.is_done,
#         }


class TodoList:
    def __init__(self, owner_name):
        self.owner_name = owner_name
        self.task_map = {}

    def add_task(self, name):
        self.task_map[name] = Task(name=name, is_done=False, date_created='04-02-2022')

    def remove_task(self, name):
        self.task_map.pop(name, None)

    def mark_done(self, name):
        self.task_map[name].is_done = True

    def mark_not_done(self, name):
        self.task_map[name].is_done = False

    def show_task(self, name):
        print(self.task_map[name])

    @property
    def action(self):
        return {
            'add_task': self.add_task,
            'remove_task': self.add_task,
            'mark_done': self.add_task,
            'mark_not_done': self.add_task,
            'show_task': self.show_task,
        }


def main():
    owner_name = 'Me'
    todo = TodoList(owner_name)
    while True:
        action = input('What todo?')
        action_func = todo.action[action]
        task_name = input()
        action_func(task_name)


main()

