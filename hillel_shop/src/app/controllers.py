from datetime import datetime

from app.db import insert_one, get_many

from app.tables import products_table


def create_product(conn, data):
    data['date_created'] = datetime.now()
    query = (
        products_table.insert().values(data).returning(products_table)
    )
    return insert_one(conn, query)


def get_products(conn):
    query = (
        products_table.select()
    )
    data = get_many(conn, query)
    result = []
    for item in data:
        result.append({
            'id': item.id,
            'name': item.name,
            'price': item.price,
            'info': item.info,
            'quantity': item.quantity,
            'date_created': item.date_created.isoformat(),
            'cart_id': item.cart_id,
        })
    return result
