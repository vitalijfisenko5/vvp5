def print_hello():
    print('Hello')


def return_hello():
    return 'hello'


# print(print_hello())

# hello = return_hello()
#
# print(hello)
import typing as t


def convert_to_int_or_none(some_int: str) -> t.Union[int, None]:
    try:
        return int(some_int)
    except Exception:
        return None


# maybe_int = convert_to_int_or_none(input('Input some digit: '))
#
# print(maybe_int)


def detect_5_in_range(some_list):
    for item in some_list:
        if item == 5:
            return item
        print(item)


# print(detect_5_in_range([1, 2, 3, 4, 5, 6, 7, 8]), '55555')
# print(detect_5_in_range([1, 2, 3, 4, 4, 6, 7, 8]))

def some_func(a, b, c):
    print(a, b, c)


# some_func(1, 3, 2)

def some_kwarg_function(a: int, b: int):
    print(a, b)


# some_kwarg_function(a=3, b=5)

def some_args_and_kwargs_func(a, b, c, d: int, e: int):
    print(a, b, c, d, e)


# some_args_and_kwargs_func(1, 2, 3, e=45, d=5)

def func_with_default(a, b=0):
    print(a, b)


# func_with_default(1, b=3)

# def func_with_default_error(a, b=None): правильный способ
#     b = b or []
#     b = [] if b is None else b
def func_with_default_error(a, b=[]):
    b.append(a)
    return b


# print(func_with_default_error(1))
# print(func_with_default_error(2))
# print(func_with_default_error(3))
# print(func_with_default_error(4))
# print(func_with_default_error(5))
# print(func_with_default_error(7, b=[]))


SOME_VAR = 1
SOME_VAR_2 = 13


def some_func_with_local_vars():
    some_local_var = 5
    print(SOME_VAR)
    print(some_local_var)
    return some_local_var


# some_local_var_copy = some_func_with_local_vars()
# print(some_local_var_copy)


def some_func_modify_global_scope():
    global SOME_VAR, SOME_VAR_2
    SOME_VAR += 10
    SOME_VAR_2 += 10


# some_func_modify_global_scope()
# print(SOME_VAR)
# print(SOME_VAR_2)


def some_func_native_args(*args):
    print(args)


# some_func_native_args(1,2,3,4,5,6,7,8,8,45)

def some_func_native_kwargs(**kwargs):
    print(kwargs)


# some_func_native_kwargs(a=3, v=4, c=4, d=4)

# def validate_input(data):
#     pass
#
#
# def do_smths(data):
#     pass
#
#
# def main():
#     data = input()
#     validate_input(data)
#     do_smths(data)
#
#
# main()


"""
1. Напишите программу, которая заполняет список пятью словами,введенными с клавиатуры, измеряет длину каждого слова
и добавляет полученное значение в другой список.
Например, список слов – ['yes', 'no', 'maybe', 'ok', 'what'],
список длин – [3, 2, 5, 2, 4].
Оба, списка, должны, выводиться, на, экран.
"""


def get_list_from_input():
    words = input('Input some words, separated by comma: ')
    return words.split(',')


def get_len_words(words_list):
    return [len(w) for w in words_list]


def main():
    words_list = get_list_from_input()
    words_list_len = get_len_words(words_list)
    print(words_list)
    print(words_list_len)


# main()
# assert get_len_words(['some', 'word', 'here', '!', '@@']) == [4, 4, 4, 1, 2]
"""
2. Поместите четные числа в начало списка, нечетные - в конец
"""


def sort_digits():
    result = []
    for digit in range(10):
        if not digit % 2 or digit == 0:
            result.insert(0, digit)
            continue
        result.append(digit)
    return result


# print(sort_digits())

"""
3. Напишите функцию sum_range(start, end), которая суммирует все целые числа от значения «start» до 
величины «end» включительно. 
Если пользователь задаст первое число большее чем второе, просто поменяйте их местами.
"""


def get_validated_user_input():
    first_num = convert_to_int_or_none(input('Input first num'))
    second_num = convert_to_int_or_none(input('Input second num'))
    if first_num is not None and second_num is not None:
        start = first_num if first_num <= second_num else second_num
        end = second_num if first_num < second_num else first_num
        return start, end
    raise ValueError


def sum_range():
    start, end = get_validated_user_input()
    return sum(range(start, end + 1))


# print(sum_range())


"""
4. Написать функцию date, принимающую 3 аргумента — день, месяц и год. Вернуть True, если такая дата
 есть в нашем календаре, и False иначе.
"""
from datetime import datetime


def date(day: int, month: int, year: int):
    try:
        datetime(day=day, month=month, year=year)
        return True
    except:
        return False


# print(date(30, 2, 2022))

"""
5. Написать функцию is_year_leap, принимающую 1 аргумент — год, и возвращающую True, если год 
високосный, и False иначе.
"""


def is_year_leap(year):
    return bool((not year % 4) or not year % 400)


# print(is_year_leap(12))


"""
6. Написать функцию, которая приймет дату и вернут выходной это или нет (без праздников)
"""

WEEKENDS = {6, 7}


def is_weekend(date: datetime):
    return date.isoweekday() in WEEKENDS

# print(is_weekend(datetime(day=9, month=1, year=2022)))
