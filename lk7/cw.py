# совсем забыл, принт тоже output операция)
# with open('aaa.txt', 'w') as f:
#     print('jfjfjjfjf', file=f)

# CSV

import csv


def main_csv():
    headers = []
    rows = []
    with open('meest_stat_based_on_delivery_service_data.csv') as f:
        reader = csv.reader(f)

        for idx, item in enumerate(reader):
            if idx == 0:
                headers = item
                continue
            rows.append(item)
    return headers, rows


# print(main_csv())

import json


def csv_writer():
    with open('new.csv', 'w') as file, open('../lk6/call_db.json') as json_file:
        writer = csv.writer(file)
        writer.writerow(['Полная стоимость', 'Триф', "Телефон", 'Старт', 'Стоп'])

        db = json.load(json_file)
        for item in db.values():
            for v in item:
                writer.writerow(v.values())


# csv_writer()
# YAML
# import yaml
# with open('test.yaml') as f:
#     print(yaml.safe_load(f))
# XLSX

import openpyxl

# s = openpyxl.load_workbook('file_example_XLSX_1000.xlsx')
#
# print(s.active[1])
#
# for cell in s.active[1]:
#    print(cell.value)
#
# for cell in s.active[2]:
#     print(cell.value)
#
#
# for _ in range(4):
#    s.active.cell(row=1, column=1, value='KKKKKKK')
#
# s.save('test.xlsx')

"""
1. Создать pdf, а потом считать его
"""
import PyPDF2  # для чтения

from reportlab.pdfgen import canvas  # - для создания
from reportlab.lib import colors


def create_pdf():
    file_name = 'some.pdf'
    document_title = 'Some data'
    title = 'History of beer'
    pdf = canvas.Canvas(file_name)
    pdf.setTitle(document_title)
    pdf.setFont('Times-Roman', 36)
    pdf.drawCentredString(290, 770, title)
    pdf.setFillColor(colors.black)
    pdf.line(30, 710, 550, 710)
    text = pdf.beginText(240, 680)
    text.setFont("Times-Roman", 18)
    text.setFillColor(colors.gold)
    text.textLine('kjndfkjnvkjdnfkvjndfjknvd')

    pdf.drawText(text)
    pdf.save()


# create_pdf()


def read_pdf():
    pdf = PyPDF2.PdfFileReader('some.pdf')
    print(pdf.getPage(0))


"""
3. Заархивировать в zip архив файлы
"""
# import zipfile as zip
#
# with zip.ZipFile('new.zip', 'w') as zip_file:
#     zip_file.write('new.csv')
#     zip_file.write('test.xlsx')
#     zip_file.write('test.yaml')
#     zip_file.setpassword(b'1234')


"""
0. В банку є можливість покласти гроші на депозит під певний відсоток і отримати прибуток через деякий час.
Наприклад, якщо покласти 10000 на 3 роки під 4% річних, отримаємо:
Перший рік: 10000 + 4% = 10400 # 10000 + 10000 * 0.04
Другий рік: 10400 + 4% = 10816 # 10400 + 10400 * 0.04
Третій рік: 10816 + 4% = 11248.64 # 10816 + 10816 * 0.04

Чистий прибуток = 11248.64 - 10000 = 1248.64
Напиши функцію calculate_profit, яка приймає три параметри:
amount - початкова сума, яку ми кладемо на депозит;
percent - річна відсоткова ставка;
period - кількість років (час, на який гроші кладуться на депозит).
Функція повинна розрахувати і повернути суму чистого прибутку за весь час.
"""


def calculate_profit(initial_amount, years, percent) -> float:
    deposit = initial_amount
    # for _ in range(years):
    #     deposit += deposit * percent
    # return round(deposit - initial_amount, 2)
    return round(deposit * ((1 + percent) ** years) - initial_amount, 2)


# print(calculate_profit(10000, 3, 0.04))
# assert calculate_profit(10000, 3, 0.04) == 1248.64
# если успеем - декораторы

def bread(func):
    def wrapper(*args, **kwargs):
        print('bread')
        func(*args, **kwargs)
        print('bread')

    return wrapper


@bread
def salami(layer):
    for _ in range(layer):
        print('salami')


salami(5)
# bread(salami)(5)


def logger(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        log = f'{func.__name__} {args=}, {kwargs=}, {result=}, now \n'
        with open('log.log', 'a') as file:
            file.write(log)
        return result

    return wrapper


@logger
def mul(a, b):
    return a * b


# mul(3,5)

# @logger
def add(a, b):
    return a + b


logger(add)(3, 5)
# https://py.checkio.org/
