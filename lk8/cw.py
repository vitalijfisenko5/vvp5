# Рекурсия))

"""
1.Написать  функцию для вычесления n-го числа фиббоначи
"""


def fib(number: int) -> int:
    if number == 1:
        return 0
    if number in {2, 3}:
        return 1
    return fib(number - 1) + fib(number - 2)


# print(fib(40))

"""
2.Написать  функцию для вычесления факториала
"""


def fact(number: int) -> int:
    if number == 0:
        return 1
    return fact(number - 1) * number


# print(fact(500))
# Сортировки

def bubble(array) -> list:
    n = len(array) - 1
    for i in range(n):
        for j in range(n - i):
            if array[j] > array[j + 1]:
                array[j], array[j + 1] = array[j + 1], array[j]
    return array


# print(bubble([10, 7, 8, 9, 1, 5]))

"""
Quick sort
____
Быстрая сортировка начинается с разбиения списка и выбора одного из элементов в качестве опорного.
А всё остальное передвигаем так, чтобы этот элемент встал на своё место. Все элементы меньше него
перемещаются влево, а равные и большие элементы перемещаются вправо.
"""


def partition(start, end, array):
    # Initializing pivot's index to start
    pivot_index = start
    pivot = array[pivot_index]

    # This loop runs till start pointer crosses
    # end pointer, and when it does we swap the
    # pivot with element on end pointer
    while start < end:

        # Increment the start pointer till it finds an
        # element greater than  pivot
        while start < len(array) and array[start] <= pivot:
            start += 1

        # Decrement the end pointer till it finds an
        # element less than pivot
        while array[end] > pivot:
            end -= 1

        # If start and end have not crossed each other,
        # swap the numbers on start and end
        if (start < end):
            array[start], array[end] = array[end], array[start]

    # Swap pivot element with element on end pointer.
    # This puts pivot on its correct sorted place.
    array[end], array[pivot_index] = array[pivot_index], array[end]

    # Returning end pointer to divide the array into 2
    return end


# The main function that implements QuickSort
def quick_sort(start, end, array):
    if (start < end):
        # p is partitioning index, array[p]
        # is at right place
        p = partition(start, end, array)

        # Sort elements before partition
        # and after partition
        quick_sort(start, p - 1, array)
        quick_sort(p + 1, end, array)


# Driver code
# array = [10, 7, 8, 9, 1, 5]
# quick_sort(0, len(array) - 1, array)
#
# print(f'Sorted array: {array}')

"""
Selection sort
____
Находится наименьший элемент и меняется с первым местами.
Теперь, когда нам известно, что первый элемент списка отсортирован, находим наименьший 
элемент из оставшихся и меняем местами со вторым. Повторяем это до тех пор, пока 
не останется последний элемент в списке.
"""

import sys


def selection(array: list) -> list:
    # Traverse through all array elements
    for i in range(len(array)):

        # Find the minimum element in remaining
        # unsorted array
        min_idx = i
        for j in range(i + 1, len(array)):
            if array[min_idx] > array[j]:
                min_idx = j

        # Swap the found minimum element with
        # the first element
        array[i], array[min_idx] = array[min_idx], array[i]
        breakpoint()

    return array


# print(selection([10, 7, 8, 9, 1, 5]))

"""
Merge sort
____
Список рекурсивно разделяется пополам, пока в итоге не получатся списки размером в один элемент. 
Массив из одного элемента считается упорядоченным. Соседние элементы сравниваются и соединяются 
вместе. Это происходит до тех пор, пока не получится полный отсортированный список.

Сортировка осуществляется путём сравнения наименьших элементов каждого подмассива. 
Первые элементы каждого подмассива сравниваются первыми. Наименьший элемент перемещается в 
результирующий массив. Счётчики результирующего массива и подмассива, откуда был взят элемент, 
увеличиваются на 1.
"""


def merge(arr):
    if len(arr) > 1:

        # Finding the mid of the array
        mid = len(arr) // 2

        # Dividing the array elements
        L = arr[:mid]

        # into 2 halves
        R = arr[mid:]

        # Sorting the first half
        merge(L)

        # Sorting the second half
        merge(R)

        i = j = k = 0

        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i] < R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        # Checking if any element was left
        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1

    return arr


# print(merge([10, 7, 8, 9, 1, 5]))

"""
Insertion sort
____
Предполагается, что первый элемент списка отсортирован. 
Переходим к следующему элементу, обозначим его х. Если х больше первого, оставляем его на своём 
месте. Если он меньше, копируем его на вторую позицию, а х устанавливаем как первый элемент.
Переходя к другим элементам несортированного сегмента, перемещаем более крупные элементы в 
отсортированном сегменте вверх по списку, пока не встретим элемент меньше x или не дойдём до 
конца списка. В первом случае x помещается на правильную позицию.
"""


# Build in sort

# print(sorted([10, 7, 8, 9, 1, 5]))
# print(sorted((10, 7, 8, 9, 1, 5)))
# print(sorted({10, 7, 8, 9, 1, 5}))
# list_1 = [10, 7, 8, 9, 1, 5]
# list_1.sort()
# print(list_1)


def sort_by_len(item):
    return len(item)


def count_j(item):
    return item.count('j')


def sort_dicts(item):
    return item.get('q') or 0


# print(sorted([{'q': 5, 'fdf': 2}, {'q': 10, 'dsjcnd': 323}], key=lambda x: x.get('q') or 0))
# aнтипаттерны
new_func = lambda x: x ** 2  # aтипатерн
# print(new_func(3))

# for i in range(10):
#     a = lambda x: x ** 2
#     print(a(i))

# print(list(map(print, [10, 7, 8, 9, 1, 5])))
# print([i > 2 for i in [10, 7, 8, 9, 1, 5]])
# Lambda


# Задачи
"""
1. Написать функцию, которая уберет уникальные элементы из списка
"""


def remove_single(array: list) -> list:
    for item in array:
        if array.count(item) == 1:
            array.pop(array.index(item))
    return array


# print(remove_single([1, 1, 2, 2, 3, 4, 4, 5, 5, 6]))

"""
2.  https://www.codewars.com/kata/61a8c3a9e5a7b9004a48ccc2/train/python
"""

DIRECTIONS = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW']


def get_direction(curr_cord: str, degree: int) -> str:
    if degree > 360:
        raise ValueError()
    if degree == 0 or degree % 360 == 0:
        return curr_cord
    idx = DIRECTIONS.index(curr_cord)
    a = degree // 45
    return DIRECTIONS[idx + a]


print(get_direction('N', -90))
